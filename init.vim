call plug#begin(stdpath('data') . '/plugged')
Plug 'voldikss/vim-floaterm'
"lsp & autocomplete
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-omni'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/vim-vsnip'
Plug 'hrsh7th/vim-vsnip-integ'
Plug 'hrsh7th/cmp-nvim-lsp-signature-help'
Plug 'onsails/lspkind-nvim'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'williamboman/nvim-lsp-installer'
"telescope
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'AckslD/nvim-neoclip.lua'
" Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'make' }
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-treesitter/playground'
"color scheme
Plug 'NLKNguyen/papercolor-theme'
Plug 'tomasiser/vim-code-dark'
"latex
Plug 'xuhdev/vim-latex-live-preview'
Plug 'lervag/vimtex'
"color name highlighting
" Plug 'norcalli/nvim-colorizer.lua'
"file-explorer
Plug 'kyazdani42/nvim-tree.lua'
"indent line
" Plug 'lukas-reineke/indent-blankline.nvim'
"tpope
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
" Plug 'tpope/vim-vinegar'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
call plug#end()

lua <<EOF
require'nvim-treesitter.configs'.setup {
ensure_installed = "all", -- one of "all", "maintained" (parsers with maintainers), or a list of languages
highlight = {
enable = true,              -- false will disable the whole extension
-- Setting this to true will run `:h syntax` and tree-sitter at the same time.
-- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
-- Using this option may slow down your editor, and you may see some duplicate highlights.
-- Instead of true it can also be a list of languages
additional_vim_regex_highlighting = false,
},
playground = {
    enable = true,
    disable = {},
    updatetime = 25, -- Debounced time for highlighting nodes in the playground from source code
    persist_queries = false, -- Whether the query persists across vim sessions
    keybindings = {
      toggle_query_editor = 'o',
      toggle_hl_groups = 'i',
      toggle_injected_languages = 't',
      toggle_anonymous_nodes = 'a',
      toggle_language_display = 'I',
      focus_language = 'f',
      unfocus_language = 'F',
      update = 'R',
      goto_node = '<cr>',
      show_help = '?',
    },
  }
}
EOF

filetype plugin on
syntax on
set hidden
au BufEnter * setlocal cursorline
au BufLeave * setlocal nocursorline
set noswapfile
set statusline=%.30F
set undofile
set wrap
set confirm

"Split manipulation 
set splitbelow
set splitright
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

"Keymapping, line numbering, search case sensitivity, tabstops
map <Space> <leader>
nnoremap j gj
nnoremap gj j
nnoremap k gk
nnoremap gk k
vnoremap j gj
vnoremap gj j
vnoremap k gk
vnoremap gk k
nnoremap \\ :vs<cr>:te<cr>i
tnoremap \\ <C-\><C-n>
inoremap fd <esc>
" inoremap { {}<Esc>ha
" inoremap [ []<Esc>ha
" inoremap ( ()<Esc>ha
" inoremap " ""<Esc>ha
" inoremap ' ''<Esc>ha
nnoremap <leader>n :noh<Esc>
nnoremap <leader>/ :%s/\<<C-r><C-w>\>/
set number
augroup numbertoggle
autocmd!
autocmd BufEnter,FocusGained,InsertLeave,WinEnter * if &nu && mode() != "i" | set rnu   | endif
autocmd BufLeave,FocusLost,InsertEnter,WinLeave   * if &nu                  | set nornu | endif
augroup END
highlight Normal guibg=none
highlight NonText guibg=none
set ignorecase
set smartcase
set so=5
set tabstop=2 softtabstop=2
set shiftwidth=2
set expandtab
"automatically swithc wd to dir of current file for all windows
autocmd BufEnter * silent! lcd %:p:h

"
"Search visual selection
vnoremap // y/\V<C-R>=escape(@",'/\')<CR><CR>

"Spellcheck
set spelllang=en,cjk
set spellsuggest=best,5
nnoremap <silent> <F11> :set spell!<cr>

"Tabline asks indices
set tabline=%!MyTabLine()  " custom tab pages line
function MyTabLine()
let s = '' " complete tabline goes here
" loop through each tab page
for t in range(tabpagenr('$'))
	" set highlight
	if t + 1 == tabpagenr()
		let s .= '%#TabLineSel#'
	else
		let s .= '%#TabLine#'
	endif
	" set the tab page number (for mouse clicks)
	let s .= '%' . (t + 1) . 'T'
	let s .= ' '
	" set page number string
	let s .= t + 1 . ' '
	" get buffer names and statuses
	let n = ''      "temp string for buffer names while we loop and check buftype
	let m = 0       " &modified counter
	let bc = len(tabpagebuflist(t + 1))     "counter to avoid last ' '
	" loop through each buffer in a tab
	for b in tabpagebuflist(t + 1)
		" buffer types: quickfix gets a [Q], help gets [H]{base fname}
		" others get 1dir/2dir/3dir/fname shortened to 1/2/3/fname
		if getbufvar( b, "&buftype" ) == 'help'
			let n .= '[H]' . fnamemodify( bufname(b), ':t:s/.txt$//' )
		elseif getbufvar( b, "&buftype" ) == 'quickfix'
			let n .= '[Q]'
		else
			let n .= pathshorten(bufname(b))
		endif
		" check and ++ tab's &modified count
		if getbufvar( b, "&modified" )
			let m += 1
		endif
		" no final ' ' added...formatting looks better done later
		if bc > 1
			let n .= ' '
		endif
		let bc -= 1
	endfor
	" add modified label [n+] where n pages in tab are modified
	if m > 0
		let s .= '[' . m . '+]'
	endif
	" select the highlighting for the buffer names
	" my default highlighting only underlines the active tab
	" buffer names.
	if t + 1 == tabpagenr()
		let s .= '%#TabLineSel#'
	else
		let s .= '%#TabLine#'
	endif
	" add buffer names
	if n == ''
		let s.= '[New]'
	else
		let s .= n
	endif
	" switch to no underlining and add final space to buffer list
	let s .= ' '
endfor
" after the last tab fill with TabLineFill and reset tab page nr
let s .= '%#TabLineFill#%T'
" right-align the label to close the current tab page
if tabpagenr('$') > 1
	let s .= '%=%#TabLineFill#%999Xclose'
endif
return s
endfunction

"""""""""""""""NETRW
"netrw on start if no args
augroup ProjectDrawer
    autocmd!
    autocmd VimEnter * if argc() == 0 | Explore! | endif
augroup END
"liststyle
let g:netrw_liststyle=3

"Telescope 
nnoremap <leader>f <cmd>Telescope find_files<CR>
nnoremap <leader>g <cmd>Telescope live_grep<CR>
nnoremap <leader>s <cmd>Telescope grep_string<CR>
nnoremap <leader>l <cmd>Telescope lsp_dynamic_workspace_symbols<CR>
nnoremap <leader>b <cmd>Telescope buffers<CR>
nnoremap <leader>h <cmd>Telescope help_tags<CR>
nnoremap <leader>p <cmd>Telescope neoclip<CR>
nnoremap <leader>rl :so ~/.config/nvim/init.vim<CR>

"Telescope + extensions
lua <<EOF
require('telescope').setup{
  defaults = {
    mappings = {
      i = {
        ["<C-h>"] = "which_key",
        ["<C-d>"] = require('telescope.actions').delete_buffer,
        ["<C-f>"] = require('telescope.actions').send_selected_to_qflist + require('telescope.actions').open_qflist,
      }
    }
  },
  pickers = {
  },
  extensions = {
      extensions = {
        require("telescope").load_extension "neoclip"
    }
  }
}
require('neoclip').setup({
  history = 1000,
  preview = true,
  keys = {
        telescope = {
          i = {
            select = '<cr>',
            paste = '<c-j>',
            paste_behind = '<c-k>',
            replay = '<c-q>',  -- replay a macro
            delete = '<c-d>',  -- delete an entry
            custom = {},
          }
        }
      }
})
EOF

"nvim-tree
lua <<EOF
require("nvim-tree").setup({
--open_on_setup_file = true,
renderer =
{
  indent_markers = {
    enable = true,},
},
})
vim.api.nvim_create_autocmd("BufEnter", {
  nested = true,
  callback = function()
    if #vim.api.nvim_list_wins() == 1 and vim.api.nvim_buf_get_name(0):match("NvimTree_") ~= nil then
      vim.cmd "quit"
    end
  end
})
EOF
nnoremap <leader>t :NvimTreeToggle<CR>
lua require'nvim-web-devicons'.setup()

"Lsp-installer
lua require('nvim-lsp-installer').setup {}
"LSP servers
lua <<EOF
local servers = {'clangd'}
for _, lsp in pairs(servers) do
  require('lspconfig')[lsp].setup {
    on_attach = on_attach,
    flags = {
      debounce_text_changes = 150,
    },
  }
end
EOF
lua require'lspconfig'.pyright.setup{}


"LSP config (the mappings used in the default file don't quite work right)
nnoremap <silent> gd <cmd>lua vim.lsp.buf.definition()<CR>
nnoremap <silent> gD <cmd>lua vim.lsp.buf.declaration()<CR>
nnoremap <silent> gr <cmd>lua vim.lsp.buf.references()<CR>
nnoremap <silent> gi <cmd>lua vim.lsp.buf.implementation()<CR>
nnoremap <silent> gz <cmd>lua vim.lsp.buf.hover()<CR>
nnoremap <silent> <C-s> <cmd>lua vim.lsp.buf.signature_help()<CR>
nnoremap <silent> <C-p> <cmd>lua vim.diagnostic.goto_prev()<CR>
nnoremap <silent> <C-n> <cmd>lua vim.diagnostic.goto_next()<CR>
nnoremap <silent> go <cmd>lua vim.lsp.buf.code_action()<CR>  
" auto-format
autocmd BufWritePre *.js lua vim.lsp.buf.formatting_sync(nil, 100)
autocmd BufWritePre *.jsx lua vim.lsp.buf.formatting_sync(nil, 100)
autocmd BufWritePre *.py lua vim.lsp.buf.formatting_sync(nil, 100)
autocmd BufWritePre *.cpp lua vim.lsp.buf.formatting_sync(nil, 100)
autocmd BufWritePre *.h lua vim.lsp.buf.formatting_sync(nil, 100)
autocmd BufWritePre *.c lua vim.lsp.buf.formatting_sync(nil, 100)

"floating lsp diagnostic & diagnostic in messege area
lua <<EOF
-- You will likely want to reduce updatetime which affects CursorHold
-- note: this setting is global and should be set only once
vim.o.updatetime = 0
vim.cmd [[autocmd! CursorHold,CursorHoldI * lua vim.diagnostic.open_float(nil, {focus=false})]]
vim.cmd [[autocmd! CursorHold,CursorHoldI * lua vim.diagnostic.open_float(nil, {focus=false, scope="cursor"})]]

vim.diagnostic.config({
  virtual_text = false,
  signs = true,
  underline = true,
  update_in_insert = false,
  severity_sort = ture,
})
EOF

""Nvim-cmp
set completeopt=menu,menuone,noselect
lua <<EOF
  -- Setup nvim-cmp.
  local cmp = require'cmp'
  local lspkind = require('lspkind')

  cmp.setup({
    snippet = {
      -- REQUIRED - you must specify a snippet engine
      expand = function(args)
        vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
        -- require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
        -- vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
        -- require'snippy'.expand_snippet(args.body) -- For `snippy` users.
      end,
    },
    mapping = {
      ['<C-d>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
      ['<C-f>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
      ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
      ['<C-y>'] = cmp.config.disable, -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
      ['<C-e>'] = cmp.mapping({
        i = cmp.mapping.abort(),
        c = cmp.mapping.close(),
      }),
      ['<CR>'] = cmp.mapping.confirm({ select = true }),
      --["<tab>"] = cmp.mapping(cmp.mapping.select_next_item(), { "i", "s", "c"}),
      --["<s-tab>"] = cmp.mapping(cmp.mapping.select_prev_item(), { "i", "s", "c"}),
      --above two lines no longer functional; apparently requires cmp.visible() check

      --https://github.com/Aumnescio/dotfiles/blob/c647e3a73150af8eb0eb0713cda2667f11c07571/nvim/init.lua#L677
      ["<C-n>"] = cmp.mapping({
          c = function()
              if cmp.visible() then
                  cmp.select_next_item({ behavior = cmp.SelectBehavior.Replace })
              else
                  vim.api.nvim_feedkeys(repterm('<Down>'), 'n', true)
              end
          end,
          i = function(fallback)
              if cmp.visible() then
                  cmp.select_next_item({ behavior = cmp.SelectBehavior.Replace })
              else
                  fallback()
              end
          end
      }),
      ["<C-p>"] = cmp.mapping({
          c = function()
              if cmp.visible() then
                  cmp.select_prev_item({ behavior = cmp.SelectBehavior.Replace })
              else
                  vim.api.nvim_feedkeys(repterm('<Up>'), 'n', true)
              end
          end,
          i = function(fallback)
              if cmp.visible() then
                  cmp.select_prev_item({ behavior = cmp.SelectBehavior.Replace })
              else
                  fallback()
              end
          end
      }),
    },
    formatting = {
	    format = lspkind.cmp_format({with_text = true, maxwidth = 50})
   },
    sources = cmp.config.sources({
      { name = 'omni' },
      { name = 'nvim_lsp' },
      { name = 'vsnip' }, -- For vsnip users.
      { name = 'nvim_lsp_signature_help' },
      -- { name = 'luasnip' }, -- For luasnip users.
      -- { name = 'ultisnips' }, -- For ultisnips users.
      -- { name = 'snippy' }, -- For snippy users.
    }, {
      { name = 'buffer' },
    })
  })

  -- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
  cmp.setup.cmdline('/', {
    sources = {
      { name = 'buffer' }
    }
  })

  -- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
  cmp.setup.cmdline(':', {
    sources = cmp.config.sources({
      { name = 'path' }
    }, {
      { name = 'cmdline' }
    })
  })

  -- Setup nvim-cmp
  local capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities())
  -- Replace <YOUR_LSP_SERVER> with each lsp server you've enabled.
  require('lspconfig')['clangd'].setup {
    capabilities = capabilities,
  }
EOF

imap <expr> <Tab>   vsnip#jumpable(1)   ? '<Plug>(vsnip-jump-next)'      : '<Tab>'
smap <expr> <Tab>   vsnip#jumpable(1)   ? '<Plug>(vsnip-jump-next)'      : '<Tab>'
imap <expr> <S-Tab> vsnip#jumpable(-1)  ? '<Plug>(vsnip-jump-prev)'      : '<S-Tab>'
smap <expr> <S-Tab> vsnip#jumpable(-1)  ? '<Plug>(vsnip-jump-prev)'      : '<S-Tab>'


"Vim-Latex
" Viewer options: One may configure the viewer either by specifying a built-in
" viewer method:
let g:vimtex_view_method = 'zathura'
let g:vimtex_complete_close_braces = 1

" VimTeX uses latexmk as the default compiler backend. If you use it, which is
" strongly recommended, you probably don't need to configure anything. If you
" want another compiler backend, you can change it as follows. The list of
" supported backends and further explanation is provided in the documentation,
" see ":help vimtex-compiler".
let g:vimtex_compiler_method = 'latexmk'
let g:vimtex_quickfix = 0

"indent line
"lua <<EOF
"require("indent_blankline").setup {
"    show_current_context = true,
"}
"EOF

"vim-floaterm
let g:floaterm_keymap_new    = '<F7>'
let g:floaterm_keymap_prev   = '<F8>'
let g:floaterm_keymap_next   = '<F9>'
let g:floaterm_keymap_toggle = '<F12>'
let g:floaterm_width = 0.9
let g:floaterm_height = 0.9

"Colour scheme
colorscheme codedark
 " set background=dark
 " colorscheme PaperColor
hi statusline ctermbg=Cyan ctermfg=Black cterm=bold

let g:livepreview_previewer = 'sioyek'

"opacity toggle
"hi! Normal ctermbg=NONE guibg=NONE
